ALOG_TEST_DIR := $(dir $(lastword $(MAKEFILE_LIST)))
ALOG_TEST_SRCS = $(shell find $(ALOG_TEST_DIR) -iname "*.c" -exec basename {} \;)
ALOG_TESTS = $(ALOG_TEST_SRCS:.c=)

$(ALOG_TESTS): $(ALOG_LIB)
	$(CC) $(CFLAGS) -g -o $(BUILD_DIR)/$@.o -c $(ALOG_TEST_DIR)$@.c
	$(CC) -o $(TARGET_DIR)/$@ $(BUILD_DIR)/$@.o $(ALOG_LIB) -lpthread
	chmod +x $(TARGET_DIR)/$@

alog-tests: $(ALOG_TESTS)
	for test in $^ ; do \
		$(TARGET_DIR)/$$test ; \
	done
