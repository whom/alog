#include "../alog.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <fcntl.h>

int out_fd[2];
int err_fd[2];

const char *out_message = "standard message";
const char *err_message = "error message";
const char *sem_name = "/alog_testing_sem";

int main() {
  // RPC sync mechanism. Start locked
  sem_t *sem = sem_open(sem_name, O_CREAT, 0x777, 0);
  if (sem_unlink(sem_name)) {
    perror("couldnt unlink semaphore");
    // might as well proceed since it is a leak anyways
  }

  // make RPC pipe
  pipe(out_fd);
  pipe(err_fd);

  // remove stdout and stderr
  _init_alog();
  alog_remove_target(1);
  alog_remove_target(2);

  pid_t childpid;
  if ((childpid = fork()) == -1) {
    perror("cannot fork");
    sem_close(sem);
    deinit_alog();
    return 1;
  }

  // child process
  if (childpid == 0) {
    close(out_fd[0]);
    close(err_fd[0]);

    // add alog targets in this pipe
    alog_add_target(out_fd[1], ALOG_OUT);
    alog_add_target(err_fd[1], ALOG_ERR);

    // write to the things
    alog(ERROR, err_message);
    alog(PRINT, out_message);

    if (sem_post(sem)) {
      perror("cannot increment semaphore");
      return 1;
    }

    // clean up
    sem_close(sem);
    deinit_alog();

  // parent process
  } else {
    // we can do checks from here
    char out_read_buffer[128] = {0};
    char err_read_buffer[128] = {0};
    if (sem_wait(sem)) {
      perror("cannot decrement semaphore");
    }

    read(err_fd[0], err_read_buffer, 128);
    // cant check read len because of timestamp
    if (!strstr(err_read_buffer, err_message)) {
      printf("err log should contain '%s'.\ninstead reads '%s'\n",
             err_message, err_read_buffer);
      return 1;
    }

    int num_out = read(out_fd[0], out_read_buffer, 128);
    if (num_out != (int) (strlen(out_message) + strlen(err_read_buffer))) {
      printf("Read %i bytes from out log when %lu were written!\n",
             num_out, strlen(out_message) + strlen(err_read_buffer) + 1);
      return 1;
    }
    if (strncmp(out_read_buffer, err_read_buffer, strlen(err_read_buffer))) {
      printf("out log first message should read '%s'.\ninstead reads '%s'\n",
             err_read_buffer, out_read_buffer);
    }
    if (strcmp(&out_read_buffer[strlen(err_read_buffer)], out_message)) {
      printf("out log second message should read '%s'.\ninstead reads '%s'\n",
             out_message, &out_read_buffer[strlen(err_read_buffer)]);
      return 1;
    }


    printf("out and err log printing are successfull.\n");
    sem_close(sem);
    deinit_alog();
  }
  return 0;
}
