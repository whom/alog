CC ?= gcc
BUILD_DIR ?= $(shell pwd)/build
TARGET_DIR ?= $(shell pwd)/target
CFLAGS = -Wall -Wextra -O2
LDFLAGS = -shared

ifdef ALOG_HIJACK_PRINTF
CFLAGS += -DALOG_HIJACK_PRINTF
endif

ifdef ALOG_DEBUG
CFLAGS += -g
endif

ALOG_TOP_DIR := $(dir $(lastword $(MAKEFILE_LIST)))
ALOG_SRC = $(ALOG_TOP_DIR)alog.c
ALOG_OBJ = $(BUILD_DIR)/alog.o
ALOG_LIB = $(TARGET_DIR)/alog.so

include $(dir $(lastword $(MAKEFILE_LIST)))tests/tests.mk

.PHONY: alog-so
alog-so: $(if $(shell stat $(ALOG_LIB)), alog-clean) $(ALOG_LIB)

$(ALOG_LIB): $(TARGET_DIR) $(ALOG_OBJ)
	$(CC) $(LDFLAGS) -o $(ALOG_LIB) $(ALOG_OBJ)

$(ALOG_OBJ): $(BUILD_DIR)
	$(CC) $(CFLAGS) -fPIC -c $(ALOG_SRC) -o $(ALOG_OBJ)

$(BUILD_DIR):
	mkdir $(BUILD_DIR)

$(TARGET_DIR):
	mkdir $(TARGET_DIR)

.PHONY: clean
alog-clean:
	rm $(ALOG_LIB) $(ALOG_OBJ)
