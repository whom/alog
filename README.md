# Introduction
- *alog* or *a logger* is a lightweight, portable logger written in C.
- It is entirely unremarkable
- Usage of *alog* is extremely simple.

### What kind of things can I log to?
Anything that is a file descriptor and can be written to with write() and fsync(). This includes the following:
- pipes
- files
- stdout
- stderr
- sockets (untested)

### How do I use this library?
- [See the API](alog.h)
- [See the tests](tests/log_test.c)

# How to build
```bash
$ make alog-so
```

# How to test
```
$ make alog-tests
```

# Variables
The following (shell) variables can be set at compile time:

- **ALOG_DEBUG**: Set this variable to compile with debug symbols
- **ALOG_HIJACK_PRINTF**: Set this variable to compile along with a printf implementation that leverages alog. Probably dont though.
