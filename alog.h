/*  ALOG: A Logger
 *  Copyright (C) 2022 Aidan Hahn
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, either version 3 of the License, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see https://www.gnu.org/licenses/.
 */

#ifndef ALOG_H
#define ALOG_H

struct _global_logging_state {
  // if set calls to alog will print to all fds in both err and out lists
  int broadcast_to_all_fds : 1;
  // if set messages with severity DEBUG will be logged
  int log_debug_messages : 1;
};

/* I know its in poor taste to exose a global like this as part of the user
 * facing API for this library but I find it way nicer than implementing and
 * using getters and setters. Since this was meant to serve my personal projects
 * I am sticking with it here.
 *
 * Alloc'ed/Initialized by a call to init_alog()*/
extern struct _global_logging_state *ALOG_LOGGING_STATE;

/* alternative impl I didnt want to write
 * it would have required a bunch of getters and setters
 * and been a PITA in general.
 * keeping it around for basically no reason.

#define LOGGING_INIT_FIELD = 0;
#define BROADCAST_TO_ALL_FIELD = 1;
#define LOG_DEBUG_MSGS_FIELD = 2;
int flags = 0; */

extern int _alog_num_out_fds;
extern int *_alog_out_fds;
extern int _alog_num_err_fds;
extern int *_alog_err_fds;

typedef enum {
  // Not printed by default. Useful for debug info
  DEBUG,
  // Default non-err message level. Prints to _alog_out_fds
  INFO,
  // Same as INFO but will get printed without the time or sev
  PRINT,
  // Default err message level. Prints to _alog_err_fds
  ERROR
} alog_sev;

/* global state is initialized with the following defaults:
 *   broadcast_to_all_fds = 0
 *   log_debug_messages = 0
 *   out fds: stdout
 *   err fds: stderr
 */
void _init_alog();
void init_alog();

/* deinitializes all global state memory used in this library */
void deinit_alog();

/* adds an fd to out fds or (iff char arg is set to 0x01) err fds */
void alog_add_target(int, char);
#define ALOG_OUT 0x00
#define ALOG_ERR 0x01

/* removes an fd from both out fds and err fds */
void alog_remove_target(int);

/* call to log a message. Takes a severity, a message, and format stuff
 * returns number of bytes written*/
int alog(alog_sev, const char *, ...);

/* overrides all calls to printf with a call to alog */
#ifdef ALOG_HIJACK_PRINTF
int printf(const char *format, ...);
#endif // ALOG_HIJACK_PRINTF

#endif // ALOG_H
